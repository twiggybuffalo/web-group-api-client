
# @lessondesk/api-client
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![make a pull request](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com) [![lessondesk-code-style](https://img.shields.io/badge/code%20style-lessondesk-ffa400.svg?style=flat-square)](https://github.com/lessondesk/eslint-config)

> Lesson Desk API client

## Table of Contents

- [Usage](#usage)
- [Install](#install)
- [Contribute](#contribute)

## Usage

```js
import api from '@lessondesk/api-client'
api.licences.licenceLearnerCount(clientId).then(console.log).catch(console.error)
```


## Install

This project uses [node](https://nodejs.org) and [npm](https://www.npmjs.com).

```sh
$ npm install @lessondesk/api-client
$ # OR
$ yarn add @lessondesk/api-client
```

## Contribute

1. Fork it and create your feature branch: `git checkout -b my-new-feature`
2. Commit your changes: `git commit -am "Add some feature"`
3. Push to the branch: `git push origin my-new-feature`
4. Submit a pull request
