const api = require('../src')

const {
  BASIC_AUTH_UNAME,
  BASIC_AUTH_PWD,
  UPSTREAM_BASE_API_PROTOCOL,
  UPSTREAM_BASE_API_URL,
  UPSTREAM_BASE_API_PORT,
} = process.env

const config = {
  auth: {
    username: BASIC_AUTH_UNAME,
    password: BASIC_AUTH_PWD
  },
  baseURL: `${UPSTREAM_BASE_API_PROTOCOL}://${UPSTREAM_BASE_API_URL}:${UPSTREAM_BASE_API_PORT}`
}

test('should get accounts', async () => {
  const res = await api.accounts.getAccounts(config)
  expect(res).toBeDefined()
  expect(Array.isArray(res)).toBeTruthy()
  const id = res[0].id
})
