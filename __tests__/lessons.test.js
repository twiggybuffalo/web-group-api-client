const api = require('../src')

const {
  BASIC_AUTH_UNAME,
  BASIC_AUTH_PWD,
  UPSTREAM_BASE_API_PROTOCOL,
  UPSTREAM_BASE_API_URL,
  UPSTREAM_BASE_API_PORT,
} = process.env

const config = {
  auth: {
    username: BASIC_AUTH_UNAME,
    password: BASIC_AUTH_PWD
  },
  baseURL: `${UPSTREAM_BASE_API_PROTOCOL}://${UPSTREAM_BASE_API_URL}:${UPSTREAM_BASE_API_PORT}`
}

test('should get lessons', async () => {
  const accounts = await api.accounts.getAccounts(config)

  const ACCOUNT_ID = accounts[0].id
  const PAGE_SIZE = 20

  const res = await api.lessons
    .query({
      accountId: ACCOUNT_ID,
      limit: PAGE_SIZE
    }, config)

  expect(res).toBeDefined()
})
