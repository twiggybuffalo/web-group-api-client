
module.exports = {
  presets: [
    [
      '@babel/env',
      {
        targets: { ie: 11 },
        useBuiltIns: false,
      },
    ],
  ],
  'plugins': ['@babel/plugin-proposal-object-rest-spread'],
  'env': {
    'test': {
      'plugins': ['@babel/plugin-transform-runtime'],
    },
  },
}
