"use strict";

var axios = require('axios');

var users = require('./routes/users');

var clients = require('./routes/clients');

var media = require('./routes/media');

var groups = require('./routes/groups');

var group = require('./routes/group');

var licences = require('./routes/licences');

var businessContacts = require('./routes/business-contacts');

var standardContacts = require('./routes/standard-contacts');

var accounts = require('./routes/accounts');

var sites = require('./routes/sites');

var messaging = require('./routes/messaging');

var roles = require('./routes/roles');

var inbox = require('./routes/inbox');

var careers = require('./routes/careers');

var countries = require('./routes/countries');

var enrollment = require('./routes/enrollment');

var marketplace = require('./routes/marketplace');

var courses = require('./routes/courses');

var lessons = require('./routes/lessons');

var uploader = require('./routes/uploader');

var support = require('./routes/support');

var files = require('./routes/files');

var hardware = require('./routes/hardware');

var surveys = require('./routes/surveys');

var peerReviews = require('./routes/peer-reviews');

var schedulers = require('./routes/schedulers');

var distributions = require('./routes/distributions');

var etagCache = require('./utils/axios-if-match-cache-intercept');

var axiosTokenIntercept = require('./utils/token-intercept');

var instance = axios.create({
  baseURL: '/api',
  headers: {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache, no-store, must-revalidate',
    Pragma: 'no-cache',
    Expires: '0'
  }
});
etagCache.axiosIfMatchCacheIntercept(instance);
axiosTokenIntercept(instance);
module.exports = {
  users: users(instance),
  clients: clients(instance),
  group: group(instance),
  groups: groups(instance),
  licences: licences(instance),
  media: media(instance),
  businessContacts: businessContacts(instance),
  standardContacts: standardContacts(instance),
  accounts: accounts(instance),
  sites: sites(instance),
  messaging: messaging(instance),
  inbox: inbox(instance),
  roles: roles(instance),
  careers: careers(instance),
  countries: countries(instance),
  enrollment: enrollment(instance),
  marketplace: marketplace(instance),
  courses: courses(instance),
  lessons: lessons(instance),
  uploader: uploader(instance),
  support: support(instance),
  files: files(instance),
  hardware: hardware(instance),
  surveys: surveys(instance),
  peerReviews: peerReviews(instance),
  schedulers: schedulers(instance),
  distributions: distributions(instance),
  etagCache: etagCache,
  instance: instance
};