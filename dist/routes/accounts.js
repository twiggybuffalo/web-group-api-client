"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getData = require('../utils/get-raw-data');

var accountsBase = '/v2/accounts';

var crud = require('../utils/crud');

module.exports = function (instance) {
  return _objectSpread({}, crud(instance, accountsBase), {
    getAccounts: function getAccounts() {
      var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return instance.get(accountsBase, config).then(getData(config.raw));
    },
    getAccountById: function getAccountById(accountId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("".concat(accountsBase, "/").concat(accountId)).then(getData(config.raw));
    },
    postAccount: function postAccount() {
      var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return instance.post(accountsBase, config).then(getData(config.raw));
    },
    activateAccount: function activateAccount(accountId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.post("".concat(accountsBase, "/").concat(accountId, "/activate"), config).then(getData(config.raw));
    },
    deactivateAccount: function deactivateAccount(accountId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.post("".concat(accountsBase, "/").concat(accountId, "/deactivate"), config).then(getData(config.raw));
    },
    getAccountCountByClientId: function getAccountCountByClientId(clientId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("".concat(accountsBase, "/count?clientId=").concat(clientId)).then(getData(config.raw));
    },
    getAccountSiteState: function getAccountSiteState(accountId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("".concat(accountsBase, "/").concat(accountId, "/state")).then(getData(config.raw));
    },
    getRolesCount: function getRolesCount(accountId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("/v2/roles/count?accountId=".concat(accountId)).then(getData(config.raw));
    },
    getCareersCount: function getCareersCount(accountId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("/v2/careers/count?accountId=".concat(accountId)).then(getData(config.raw));
    },
    getCoursesCount: function getCoursesCount(accountId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("/v2/content/formal/courses/count?accountId=".concat(accountId)).then(getData(config.raw));
    },
    getLessonsCount: function getLessonsCount(accountId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("/v2/content/formal/lessons/count?accountId=".concat(accountId)).then(getData(config.raw));
    },
    sitesWithNoLearners: function sitesWithNoLearners(config) {
      return instance.get("/v2/sites/learner/deficits", config).then(getData(config.raw));
    },
    rolesWithNoContent: function rolesWithNoContent(config) {
      return instance.get("/v2/roles/count", config).then(getData(config.raw));
    },
    contentPendingApproval: function contentPendingApproval(type, config) {
      return instance.get("/v2/content/formal/".concat(type, "/count"), config).then(getData(config.raw));
    }
  });
};