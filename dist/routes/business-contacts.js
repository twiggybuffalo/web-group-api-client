"use strict";

var crud = require('../utils/crud');

module.exports = function (instance) {
  return crud(instance, '/v2/contacts/business');
};