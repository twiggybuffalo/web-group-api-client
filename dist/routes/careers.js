"use strict";

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var crud = require('../utils/crud');

var getData = require('../utils/get-data');

var careersEndpoint = '/v2/careers';

module.exports = function (instance) {
  return _objectSpread({}, crud(instance, careersEndpoint), {
    linkRoles: function linkRoles(id, data) {
      return instance.put("".concat(careersEndpoint, "/").concat(id, "/roles"), data);
    },
    getAssignedCareers: function getAssignedCareers() {
      var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return instance.get("".concat(careersEndpoint, "/assigned"), config).then(getData);
    },
    getRolesByCareer: function getRolesByCareer(id) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("".concat(careersEndpoint, "/").concat(id, "/roles"), config).then(getData);
    },
    enrollUser: function enrollUser(data) {
      return instance.post("".concat(careersEndpoint, "/enroll"), data).then(getData);
    },
    unenrollUser: function unenrollUser(data) {
      return instance.post("".concat(careersEndpoint, "/unenroll"), data).then(getData);
    },
    changeState: function changeState(id, config) {
      var from = config.from,
          to = config.to,
          data = _objectWithoutProperties(config, ["from", "to"]);

      return instance.post("".concat(careersEndpoint, "/").concat(id, "/state?from=").concat(from, "&to=").concat(to), data);
    }
  });
};