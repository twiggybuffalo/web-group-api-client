"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getData = require('../utils/get-raw-data');

var crud = require('../utils/crud');

var baseRoute = '/v2/clients';

module.exports = function (instance) {
  return _objectSpread({}, crud(instance, baseRoute), {
    getClients: function getClients(params) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      config.params = params;
      return instance.get(baseRoute, config).then(getData(config.raw));
    },
    getClientById: function getClientById(clientId) {
      return instance.get("".concat(baseRoute, "/").concat(clientId)).then(function (res) {
        res.data.data.ifMatch = res.headers.etag;
        return res.data.data;
      });
    },
    toggleActiveState: function toggleActiveState(clientId, active) {
      var config = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      return instance.post("".concat(baseRoute, "/").concat(clientId, "/").concat(active ? 'deactivate' : 'activate'), config).then(getData(config.raw));
    },
    getAccountsForClient: function getAccountsForClient() {
      var clientId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("".concat(baseRoute, "/").concat(clientId, "/accounts")).then(getData(config.raw));
    },
    getSitesForClient: function getSitesForClient() {
      var clientId = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      var accountId = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
      var config = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      return instance.get("".concat(baseRoute, "/").concat(clientId, "/accounts/").concat(accountId, "/sites")).then(getData(config.raw));
    },
    createClient: function createClient(data) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.post(baseRoute, data, config).then(getData(config.raw));
    },
    getLanguages: function getLanguages() {
      var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return instance.get("".concat(baseRoute, "/languages"), config).then(getData(config.raw));
    },
    searchClients: function searchClients(params) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("".concat(baseRoute, "/search"), {
        params: params
      }).then(getData(config.raw));
    }
  });
};