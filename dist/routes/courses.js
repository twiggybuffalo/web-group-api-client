"use strict";

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var crud = require('../utils/crud');

function courseRoutes(instance, baseRoute, type) {
  return _objectSpread({}, crud(instance, "".concat(baseRoute, "/").concat(type, "/courses")), {
    changeState: function changeState(id, config) {
      var from = config.from,
          to = config.to,
          data = _objectWithoutProperties(config, ["from", "to"]);

      return instance.post("".concat(baseRoute, "/").concat(type, "/courses/").concat(id, "/state?from=").concat(from, "&to=").concat(to), data);
    }
  });
}

module.exports = function (instance) {
  var baseRoute = "/v2/content";

  var putLessons = function putLessons(id, lessonIds, type) {
    var config = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
    return instance.put("".concat(baseRoute, "/").concat(type, "/courses/").concat(id, "/lessons"), lessonIds, config);
  };

  return {
    onboarding: courseRoutes(instance, baseRoute, 'onboarding'),
    formal: courseRoutes(instance, baseRoute, 'formal'),
    putLessons: putLessons
  };
};