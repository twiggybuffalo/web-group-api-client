"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var crud = require('../utils/crud');

var getData = require('../utils/get-raw-data');

function distributionRoutes(instance, type) {
  var baseRoute = "/v2/distributions/".concat(type);
  return _objectSpread({
    dist: crud(instance, "".concat(baseRoute, "/distributions"))
  }, crud(instance, baseRoute), {
    distribute: function distribute() {
      var body = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var extractNestedData = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return instance.post("".concat(baseRoute, "/distribute"), body, config).then(getData(!extractNestedData));
    },
    getStats: function getStats(id) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var extractNestedData = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return instance.get("".concat(baseRoute, "/distributions/").concat(id, "/stats"), config).then(getData(!extractNestedData));
    }
  });
}

module.exports = function (instance) {
  return {
    documents: distributionRoutes(instance, 'documents'),
    newsflashes: distributionRoutes(instance, 'newsflashes')
  };
};