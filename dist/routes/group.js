"use strict";

var _axios = _interopRequireDefault(require("axios"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getData = require('../utils/get-data');

var credentialsInstance = _axios.default.create({
  baseURL: '/api',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
});

module.exports = function (instance) {
  return {
    credentials: function credentials(keycloakURL, realm, params) {
      return credentialsInstance.post("".concat(keycloakURL, "/realms/").concat(realm, "/protocol/openid-connect/token"), params).then(getData);
    },
    sync: function sync(hardware) {
      return instance.post('/v2/hardware/devices/sync', hardware).then(getData);
    },
    assessments: function assessments(assessmentId) {
      return instance.get("/v2/content/formal/assessments/".concat(assessmentId)).then(getData);
    },
    video: function video(lessonId) {
      return instance.get("/v2/content/formal/lessons/".concat(lessonId, "/video")).then(getData);
    }
  };
};