"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var crud = require('../utils/crud');

var getData = require('../utils/get-data');

var baseRoute = '/v2/groups';

module.exports = function (instance) {
  return _objectSpread({}, crud(instance, baseRoute), {
    getGroups: function getGroups(accountId, clientId) {
      return instance.get("".concat(baseRoute, "?clientId=").concat(clientId, "&accountId=").concat(accountId)).then(getData);
    },
    getUsers: function getUsers(groupId) {
      return instance.get("".concat(baseRoute, "/").concat(groupId, "/users")).then(getData);
    },
    addRoles: function addRoles(groupId, roles) {
      return instance.post("".concat(baseRoute, "/").concat(groupId, "/roles"), roles);
    },
    addUsers: function addUsers(groupId, users) {
      return instance.post("".concat(baseRoute, "/").concat(groupId, "/users"), users);
    },
    removeRoles: function removeRoles(groupId, roles) {
      return instance.delete("".concat(baseRoute, "/").concat(groupId, "/roles"), {
        data: roles
      });
    },
    removeUsers: function removeUsers(groupId, users) {
      return instance.delete("".concat(baseRoute, "/").concat(groupId, "/users"), {
        data: users
      });
    },
    deleteGroup: function deleteGroup(groupId) {
      return instance.delete("".concat(baseRoute, "/").concat(groupId));
    }
  });
};