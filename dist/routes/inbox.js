"use strict";

var crud = require('../utils/crud');

var getData = require('../utils/get-raw-data');

var baseRoute = '/v2/inbox';

function createInboxRoutes(instance, suffix) {
  return {
    get: function get(userId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("".concat(baseRoute, "/").concat(userId, "/").concat(suffix), config).then(getData(config.raw));
    },
    view: function view(userId, body, config) {
      return instance.post("".concat(baseRoute, "/").concat(userId, "/").concat(suffix, "/view"), body, config);
    }
  };
}

module.exports = function (instance) {
  return {
    documents: createInboxRoutes(instance, 'documents'),
    newsflashes: createInboxRoutes(instance, 'newsflashes')
  };
};