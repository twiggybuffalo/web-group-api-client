"use strict";

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var crud = require('../utils/crud');

var getData = require('../utils/get-raw-data');

var baseRoute = '/v2/licences';

module.exports = function (instance) {
  return _objectSpread({
    content: crud(instance, "".concat(baseRoute, "/content"))
  }, crud(instance, baseRoute), {
    updateLicence: function updateLicence() {
      var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var id = config.id,
          type = config.type,
          body = config.body,
          params = _objectWithoutProperties(config, ["id", "type", "body"]);

      return instance.post("".concat(baseRoute, "/").concat(id, "/").concat(type), body, params).then(getData(config.raw));
    },
    activateContentLicence: function activateContentLicence() {
      var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var id = config.id,
          body = config.body,
          params = _objectWithoutProperties(config, ["id", "body"]);

      return instance.post("".concat(baseRoute, "/content/").concat(id, "/activate"), body, params);
    },
    licenceLearnerCount: function licenceLearnerCount() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      config.params = params;
      return instance.get("".concat(baseRoute, "/learner/count"), config).then(getData(config.raw));
    },
    licenceHardwareCount: function licenceHardwareCount() {
      var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      config.params = params;
      return instance.get("".concat(baseRoute, "/hardware/count"), config).then(getData(config.raw));
    },
    accountLicenceHardwareCount: function accountLicenceHardwareCount(accountId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("/v2/hardware/devices/state?accountId=".concat(accountId)).then(getData(config.raw));
    },
    getExpiring: function getExpiring(type, config) {
      return instance.get("".concat(baseRoute, "/").concat(type, "/count/expiring"), config).then(getData(config.raw));
    }
  });
};