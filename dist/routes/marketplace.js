"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var crud = require('../utils/crud');

var baseRoute = 'v2/marketplace';
var listingsRoute = "".concat(baseRoute, "/listings");
var vendorsRoute = "".concat(baseRoute, "/vendors");

module.exports = function (instance) {
  return {
    listings: _objectSpread({}, crud(instance, listingsRoute), {
      filters: function filters() {
        var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        return instance.get("".concat(listingsRoute, "/filters"), config);
      },
      search: function search() {
        var body = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return instance.post("".concat(listingsRoute, "/search"), body, config);
      },
      availability: function availability() {
        var body = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return instance.post("".concat(listingsRoute, "/availability"), body, config);
      },
      featured: function featured() {
        var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        return instance.get("".concat(listingsRoute, "/featured"), config);
      },
      contentRequest: function contentRequest(id) {
        var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return instance.post("".concat(listingsRoute, "/").concat(id, "/request"), config);
      }
    }),
    vendors: crud(instance, vendorsRoute),
    categories: function categories() {
      var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return instance.get("".concat(baseRoute, "/categories/stats"), config);
    },
    getLesson: function getLesson(id) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("".concat(baseRoute, "/lessons/").concat(id), config);
    },
    getCourse: function getCourse(id) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("".concat(baseRoute, "/courses/").concat(id), config);
    }
  };
};