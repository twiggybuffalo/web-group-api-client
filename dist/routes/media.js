"use strict";

var crud = require('../utils/crud');

module.exports = function (instance) {
  return {
    production: crud(instance, '/v2/files/productions'),
    document: crud(instance, '/v2/distributions/documents'),
    newsflash: crud(instance, '/v2/distributions/newsflashes'),
    audio: crud(instance, '/v2/files/audio'),
    image: crud(instance, '/v2/files/images'),
    subtitle: crud(instance, '/v2/files/subtitles'),
    video: crud(instance, '/v2/files/videos')
  };
};