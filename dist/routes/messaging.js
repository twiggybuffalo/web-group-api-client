"use strict";

var getData = require('../utils/get-data');

module.exports = function (instance) {
  return {
    find: function find(subjectId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("/v2/inbox/".concat(subjectId, "/messages"), config).then(getData);
    },
    get: function get(subjectId, messageId) {
      var config = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      // NOTE:  getting the message has a side-effect of marking it as read,
      //        thus we use a post request.
      return instance.post("/v2/inbox/".concat(subjectId, "/messages/").concat(messageId), config).then(getData);
    },
    markAllRead: function markAllRead(subjectId) {
      return instance.post("/v2/inbox/".concat(subjectId, "/messages/read")).then(getData);
    }
  };
};