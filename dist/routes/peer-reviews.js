"use strict";

var crud = require('../utils/crud');

var baseRoute = '/v2/reviews';

module.exports = function (instance) {
  return crud(instance, baseRoute);
};