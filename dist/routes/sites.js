"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var crud = require('../utils/crud');

var getData = require('../utils/get-data');

var baseRoute = '/v2/sites';

module.exports = function (instance) {
  return _objectSpread({}, crud(instance, baseRoute), {
    filters: function filters(accountId) {
      return instance.get("".concat(baseRoute, "/filters"), {
        params: {
          accountId: accountId
        }
      }).then(getData);
    },
    activate: function activate(id) {
      return instance.post("".concat(baseRoute, "/").concat(id, "/activate"));
    },
    deactivate: function deactivate(id) {
      return instance.post("".concat(baseRoute, "/").concat(id, "/deactivate"));
    },
    assignLearner: function assignLearner(siteId, userIds) {
      return instance.post("".concat(baseRoute, "/").concat(siteId, "/assign"), {
        userIds: userIds
      });
    },
    unassignLearner: function unassignLearner(siteId, userIds) {
      return instance.post("".concat(baseRoute, "/").concat(siteId, "/unassign"), {
        userIds: userIds
      });
    },
    assigned: function assigned(config) {
      return instance.get("".concat(baseRoute, "/assigned"), config).then(getData);
    },
    assignedUsers: function assignedUsers(id, config) {
      return instance.get("".concat(baseRoute, "/").concat(id, "/users"), config).then(getData);
    },
    assignedById: function assignedById(id, config) {
      return instance.get("".concat(baseRoute, "/assigned/").concat(id), config).then(getData);
    }
  });
};