"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var crud = require('../utils/crud');

var getData = require('../utils/get-raw-data');

var baseRoute = '/v2/support';

module.exports = function (instance) {
  return {
    faq: _objectSpread({}, crud(instance, "".concat(baseRoute, "/faqs"))),
    glossary: _objectSpread({}, crud(instance, "".concat(baseRoute, "/terms"))),
    manuals: _objectSpread({}, crud(instance, "".concat(baseRoute, "/manuals"))),
    videos: _objectSpread({}, crud(instance, "".concat(baseRoute, "/videos"))),
    steps: _objectSpread({}, crud(instance, "".concat(baseRoute, "/steps"))),
    orderedFiles: _objectSpread({}, crud(instance, "".concat(baseRoute, "/orderedfiles"))),
    downloadMedia: {
      get: function get(uri) {
        var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return instance.get(uri, config).then(getData(config.raw));
      }
    }
  };
};