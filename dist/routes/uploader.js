"use strict";

var baseUrl = '/v2/uploader';

module.exports = function (instance) {
  return {
    bulkUploadLearners: function bulkUploadLearners(accountId, data, config) {
      return instance.put("".concat(baseUrl, "/learners/").concat(accountId), data, config);
    }
  };
};