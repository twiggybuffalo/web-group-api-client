"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getData = require('../utils/get-raw-data');

var crud = require('../utils/crud');

var baseRoute = '/v2/users';

module.exports = function (instance) {
  return _objectSpread({}, crud(instance, baseRoute), {
    getGroupsByUserById: function getGroupsByUserById(userId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.get("".concat(baseRoute, "/").concat(userId, "/groups"), config).then(getData(config.raw));
    },
    getUserCount: function getUserCount() {
      var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return instance.get("".concat(baseRoute, "/count"), config).then(getData(config.raw));
    },
    addRoleToLearner: function addRoleToLearner(learnerId, roles) {
      return instance.post("".concat(baseRoute, "/").concat(learnerId, "/roles"), roles);
    },
    removeRoleFromLearner: function removeRoleFromLearner(learnerId, roles) {
      return instance.delete("".concat(baseRoute, "/").concat(learnerId, "/roles"), {
        data: roles
      });
    },
    activate: function activate(id) {
      var _activate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

      var config = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      var action = _activate ? 'activate' : 'deactivate';
      return instance.post("".concat(baseRoute, "/").concat(id, "/").concat(action)).then(getData(config.raw));
    },
    removeLearnerRole: function removeLearnerRole(learnerId) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return instance.delete("".concat(baseRoute, "/").concat(learnerId, "/roles"), {
        data: ['user:learner']
      }, config);
    }
  });
};