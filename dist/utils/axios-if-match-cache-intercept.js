"use strict";

var etagMap = new Map();

function isCacheableMethod() {
  var method = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
  return ['GET', 'HEAD'].includes(method.toUpperCase());
}

function isIfMatchMethod(method) {
  return ['PUT', 'PATCH'].includes(method.toUpperCase());
}

function axiosIfMatchCacheIntercept(axios) {
  axios.interceptors.request.use(function (config) {
    if (isIfMatchMethod(config.method)) {
      var url = config.url;
      var etag = etagMap.get("/api".concat(url));

      if (etag) {
        config.headers['If-Match'] = etag.replace(new RegExp('"', 'g'), '');
        config.headers['Content-Type'] = 'application/merge-patch+json';
      }
    }

    return config;
  }); // Add a response interceptor

  axios.interceptors.response.use(function (response) {
    if (isCacheableMethod(response.config.method)) {
      var etag = response.headers.etag;

      if (etag) {
        etagMap.set(response.config.url, etag);
      }
    }

    return response;
  });
  return axios;
}

module.exports = {
  axiosIfMatchCacheIntercept: axiosIfMatchCacheIntercept,
  clear: function clear() {
    return etagMap.clear();
  },
  set: function set(url, etag) {
    etagMap.set(url, etag);
  },
  get: function get(url) {
    return etagMap.get("/api".concat(url));
  }
};