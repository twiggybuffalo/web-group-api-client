"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getData = require('../utils/get-raw-data');

function crud(instance, baseRoute) {
  return {
    query: function query(params) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var query = params.query;

      if (query) {
        return instance.get("".concat(baseRoute, "/search"), _objectSpread({
          params: params
        }, config));
      }

      return instance.get(baseRoute, _objectSpread({
        params: params
      }, config));
    },
    getById: function getById(id) {
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var extractNestedData = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return instance.get("".concat(baseRoute, "/").concat(id), config).then(getData(!extractNestedData));
    },
    getMultiple: function getMultiple() {
      var ids = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var extractNestedData = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return Promise.all(ids.map(function (id) {
        return instance.get("".concat(baseRoute, "/").concat(id), config).then(getData(!extractNestedData));
      }));
    },
    create: function create() {
      var body = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var extractNestedData = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return instance.post(baseRoute, body, config).then(getData(!extractNestedData));
    },
    createMultiple: function createMultiple() {
      var objects = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var extractNestedData = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return Promise.all(objects.map(function (obj) {
        return instance.post(baseRoute, obj, config).then(getData(!extractNestedData));
      }));
    },
    patch: function patch(id) {
      var body = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var prevConfig = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      var extractNestedData = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

      var config = _objectSpread({
        headers: {
          'Content-Type': 'application/merge-patch+json'
        }
      }, prevConfig);

      return instance.patch("".concat(baseRoute, "/").concat(id), body, config).then(getData(!extractNestedData));
    },
    put: function put(id) {
      var body = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var config = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      var extractNestedData = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;
      return instance.put("".concat(baseRoute, "/").concat(id), body, config).then(getData(!extractNestedData));
    },
    delete: function _delete(id) {
      return instance.delete("".concat(baseRoute, "/").concat(id));
    },
    deleteMultiple: function deleteMultiple() {
      var ids = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      return Promise.all(ids.map(function (id) {
        return instance.delete("".concat(baseRoute, "/").concat(id));
      }));
    },
    export: function _export(exportAs, params, type) {
      var exportType = type ? "?type=".concat(type) : '';
      return instance.post("".concat(baseRoute, "/export/").concat(exportAs).concat(exportType), params);
    }
  };
}

module.exports = crud;