"use strict";

module.exports = function getData(res) {
  var data = res.data;

  if (data.data) {
    return data.data;
  }

  return data;
};