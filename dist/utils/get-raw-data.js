"use strict";

module.exports = function getData() {
  var raw = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  return function (res) {
    if (raw) {
      return res;
    }

    var data = res.data;

    if (data.data || data.data === 0) {
      return data.data;
    }

    return data;
  };
};