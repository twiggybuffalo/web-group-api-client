"use strict";

function axiosTokenIntercept(axios) {
  axios.interceptors.request.use(function (config) {
    if (typeof window !== 'undefined') {
      var token = window.localStorage.getItem('kc_token');

      if (token) {
        config.headers['Authorization'] = "Bearer ".concat(token);
      }
    }

    return config;
  });
  return axios;
}

module.exports = axiosTokenIntercept;