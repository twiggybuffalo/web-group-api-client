const axios = require('axios')

const users = require('./routes/users')
const clients = require('./routes/clients')
const media = require('./routes/media')
const groups = require('./routes/groups')
const group = require('./routes/group')
const licences = require('./routes/licences')
const businessContacts = require('./routes/business-contacts')
const standardContacts = require('./routes/standard-contacts')
const accounts = require('./routes/accounts')
const sites = require('./routes/sites')
const messaging = require('./routes/messaging')
const roles = require('./routes/roles')
const inbox = require('./routes/inbox')
const careers = require('./routes/careers')
const countries = require('./routes/countries')
const enrollment = require('./routes/enrollment')
const marketplace = require('./routes/marketplace')
const courses = require('./routes/courses')
const lessons = require('./routes/lessons')
const uploader = require('./routes/uploader')
const support = require('./routes/support')
const files = require('./routes/files')
const hardware = require('./routes/hardware')
const surveys = require('./routes/surveys')
const peerReviews = require('./routes/peer-reviews')
const schedulers = require('./routes/schedulers')
const distributions = require('./routes/distributions')

const etagCache = require('./utils/axios-if-match-cache-intercept')
const axiosTokenIntercept = require('./utils/token-intercept')

const instance = axios.create({
  baseURL: '/api',
  headers: {
    'Content-Type': 'application/json',
    'Cache-Control': 'no-cache, no-store, must-revalidate',
    Pragma: 'no-cache',
    Expires: '0',
  },
})

etagCache.axiosIfMatchCacheIntercept(instance)
axiosTokenIntercept(instance)

module.exports = {
  users: users(instance),
  clients: clients(instance),
  group: group(instance),
  groups: groups(instance),
  licences: licences(instance),
  media: media(instance),
  businessContacts: businessContacts(instance),
  standardContacts: standardContacts(instance),
  accounts: accounts(instance),
  sites: sites(instance),
  messaging: messaging(instance),
  inbox: inbox(instance),
  roles: roles(instance),
  careers: careers(instance),
  countries: countries(instance),
  enrollment: enrollment(instance),
  marketplace: marketplace(instance),
  courses: courses(instance),
  lessons: lessons(instance),
  uploader: uploader(instance),
  support: support(instance),
  files: files(instance),
  hardware: hardware(instance),
  surveys: surveys(instance),
  peerReviews: peerReviews(instance),
  schedulers: schedulers(instance),
  distributions: distributions(instance),
  etagCache,
  instance,
}
