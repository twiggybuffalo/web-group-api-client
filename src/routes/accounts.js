const getData = require('../utils/get-raw-data')
const accountsBase = '/v2/accounts'
const crud = require('../utils/crud')

module.exports = function(instance) {
  return {
    ...crud(instance, accountsBase),
    getAccounts(config = {}) {
      return instance.get(accountsBase, config).then(getData(config.raw))
    },

    getAccountById(accountId, config = {}) {
      return instance
        .get(`${accountsBase}/${accountId}`)
        .then(getData(config.raw))
    },

    postAccount(config = {}) {
      return instance.post(accountsBase, config).then(getData(config.raw))
    },

    activateAccount(accountId, config = {}) {
      return instance
        .post(`${accountsBase}/${accountId}/activate`, config)
        .then(getData(config.raw))
    },

    deactivateAccount(accountId, config = {}) {
      return instance
        .post(`${accountsBase}/${accountId}/deactivate`, config)
        .then(getData(config.raw))
    },

    getAccountCountByClientId(clientId, config = {}) {
      return instance
        .get(`${accountsBase}/count?clientId=${clientId}`)
        .then(getData(config.raw))
    },

    getAccountSiteState(accountId, config = {}) {
      return instance
        .get(`${accountsBase}/${accountId}/state`)
        .then(getData(config.raw))
    },

    getRolesCount(accountId, config = {}) {
      return instance
        .get(`/v2/roles/count?accountId=${accountId}`)
        .then(getData(config.raw))
    },

    getCareersCount(accountId, config = {}) {
      return instance
        .get(`/v2/careers/count?accountId=${accountId}`)
        .then(getData(config.raw))
    },

    getCoursesCount(accountId, config = {}) {
      return instance
        .get(`/v2/content/formal/courses/count?accountId=${accountId}`)
        .then(getData(config.raw))
    },

    getLessonsCount(accountId, config = {}) {
      return instance
        .get(`/v2/content/formal/lessons/count?accountId=${accountId}`)
        .then(getData(config.raw))
    },

    sitesWithNoLearners(config) {
      return instance
        .get(`/v2/sites/learner/deficits`, config)
        .then(getData(config.raw))
    },

    rolesWithNoContent(config) {
      return instance.get(`/v2/roles/count`, config).then(getData(config.raw))
    },

    contentPendingApproval(type, config) {
      return instance
        .get(`/v2/content/formal/${type}/count`, config)
        .then(getData(config.raw))
    },
  }
}
