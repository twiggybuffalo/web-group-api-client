const crud = require('../utils/crud')
const getData = require('../utils/get-data')

const careersEndpoint = '/v2/careers'

module.exports = instance => ({
  ...crud(instance, careersEndpoint),
  linkRoles(id, data) {
    return instance.put(`${careersEndpoint}/${id}/roles`, data)
  },
  getAssignedCareers(config = {}) {
    return instance.get(`${careersEndpoint}/assigned`, config).then(getData)
  },
  getRolesByCareer(id, config = {}) {
    return instance.get(`${careersEndpoint}/${id}/roles`, config).then(getData)
  },
  enrollUser(data) {
    return instance.post(`${careersEndpoint}/enroll`, data).then(getData)
  },
  unenrollUser(data) {
    return instance.post(`${careersEndpoint}/unenroll`, data).then(getData)
  },
  changeState(id, config) {
    const { from, to, ...data } = config
    return instance.post(
      `${careersEndpoint}/${id}/state?from=${from}&to=${to}`,
      data
    )
  },
})
