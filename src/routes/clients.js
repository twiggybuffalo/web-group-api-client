const getData = require('../utils/get-raw-data')
const crud = require('../utils/crud')

const baseRoute = '/v2/clients'

module.exports = function(instance) {
  return {
    ...crud(instance, baseRoute),

    getClients(params, config = {}) {
      config.params = params
      return instance.get(baseRoute, config).then(getData(config.raw))
    },

    getClientById(clientId) {
      return instance.get(`${baseRoute}/${clientId}`).then(res => {
        res.data.data.ifMatch = res.headers.etag
        return res.data.data
      })
    },

    toggleActiveState(clientId, active, config = {}) {
      return instance
        .post(
          `${baseRoute}/${clientId}/${active ? 'deactivate' : 'activate'}`,
          config
        )
        .then(getData(config.raw))
    },

    getAccountsForClient(clientId = '', config = {}) {
      return instance
        .get(`${baseRoute}/${clientId}/accounts`)
        .then(getData(config.raw))
    },

    getSitesForClient(clientId = '', accountId = '', config = {}) {
      return instance
        .get(`${baseRoute}/${clientId}/accounts/${accountId}/sites`)
        .then(getData(config.raw))
    },

    createClient(data, config = {}) {
      return instance.post(baseRoute, data, config).then(getData(config.raw))
    },

    getLanguages(config = {}) {
      return instance
        .get(`${baseRoute}/languages`, config)
        .then(getData(config.raw))
    },

    searchClients(params, config = {}) {
      return instance
        .get(`${baseRoute}/search`, { params })
        .then(getData(config.raw))
    },
  }
}
