const getData = require('../utils/get-data')
const crud = require('../utils/crud')

module.exports = instance => ({
  ...crud(instance, '/v2/countries'),
  names(config = {}, extractNestedData = true) {
    const request = instance.get(`/v2/countries`, config)
    return extractNestedData ? request.then(getData) : request
  },
})
