const crud = require('../utils/crud')

function courseRoutes(instance, baseRoute, type) {
  return {
    ...crud(instance, `${baseRoute}/${type}/courses`),

    changeState(id, config) {
      const { from, to, ...data } = config
      return instance.post(
        `${baseRoute}/${type}/courses/${id}/state?from=${from}&to=${to}`,
        data
      )
    },
  }
}

module.exports = function(instance) {
  const baseRoute = `/v2/content`

  const putLessons = (id, lessonIds, type, config = {}) =>
    instance.put(
      `${baseRoute}/${type}/courses/${id}/lessons`,
      lessonIds,
      config
    )

  return {
    onboarding: courseRoutes(instance, baseRoute, 'onboarding'),
    formal: courseRoutes(instance, baseRoute, 'formal'),
    putLessons,
  }
}
