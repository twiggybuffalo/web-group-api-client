const crud = require('../utils/crud')
const getData = require('../utils/get-raw-data')

function distributionRoutes(instance, type) {
  const baseRoute = `/v2/distributions/${type}`
  return {
    dist: crud(instance, `${baseRoute}/distributions`),
    ...crud(instance, baseRoute),

    distribute(body = {}, config = {}, extractNestedData = true) {
      return instance
        .post(`${baseRoute}/distribute`, body, config)
        .then(getData(!extractNestedData))
    },

    getStats(id, config = {}, extractNestedData = true) {
      return instance
        .get(`${baseRoute}/distributions/${id}/stats`, config)
        .then(getData(!extractNestedData))
    },
  }
}

module.exports = function(instance) {
  return {
    documents: distributionRoutes(instance, 'documents'),
    newsflashes: distributionRoutes(instance, 'newsflashes'),
  }
}
