const getData = require('../utils/get-data')
const crud = require('../utils/crud')

module.exports = instance => ({
  ...crud(instance, '/v2/enrollments'),

  getEnrolledContent(userId, config = {}) {
    return instance
      .get(`/v2/enrollments/users/${userId}/assigned`, config)
      .then(getData)
  },
})
