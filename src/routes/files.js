const crud = require('../utils/crud')
const getData = require('../utils/get-raw-data')

module.exports = function(instance) {
  return {
    ...crud(instance, '/v2/files'),

    upload(type, formData, config = {}) {
      return instance.put(`/v2/files/${type}/files`, formData, config)
    },

    createMediaProduction(data, config = {}) {
      return instance
        .post(`/v2/files/productions`, data, config)
        .then(getData(config.raw))
    },

    postFileMeta(type, data, config = {}) {
      return instance
        .post(`/v2/files/${type}`, data, config)
        .then(getData(config.raw))
    },
  }
}
