import axios from 'axios'

const getData = require('../utils/get-data')

const credentialsInstance = axios.create({
  baseURL: '/api',
  headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
})

module.exports = instance => ({
  credentials(keycloakURL, realm, params) {
    return credentialsInstance
      .post(
        `${keycloakURL}/realms/${realm}/protocol/openid-connect/token`,
        params
      )
      .then(getData)
  },

  sync(hardware) {
    return instance.post('/v2/hardware/devices/sync', hardware).then(getData)
  },

  assessments(assessmentId) {
    return instance
      .get(`/v2/content/formal/assessments/${assessmentId}`)
      .then(getData)
  },

  video(lessonId) {
    return instance
      .get(`/v2/content/formal/lessons/${lessonId}/video`)
      .then(getData)
  },
})
