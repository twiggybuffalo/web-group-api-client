const crud = require('../utils/crud')
const getData = require('../utils/get-data')

const baseRoute = '/v2/groups'

module.exports = function(instance) {
  return {
    ...crud(instance, baseRoute),

    getGroups(accountId, clientId) {
      return instance
        .get(`${baseRoute}?clientId=${clientId}&accountId=${accountId}`)
        .then(getData)
    },

    getUsers(groupId) {
      return instance.get(`${baseRoute}/${groupId}/users`).then(getData)
    },

    addRoles(groupId, roles) {
      return instance.post(`${baseRoute}/${groupId}/roles`, roles)
    },

    addUsers(groupId, users) {
      return instance.post(`${baseRoute}/${groupId}/users`, users)
    },

    removeRoles(groupId, roles) {
      return instance.delete(`${baseRoute}/${groupId}/roles`, { data: roles })
    },

    removeUsers(groupId, users) {
      return instance.delete(`${baseRoute}/${groupId}/users`, { data: users })
    },

    deleteGroup(groupId) {
      return instance.delete(`${baseRoute}/${groupId}`)
    },
  }
}
