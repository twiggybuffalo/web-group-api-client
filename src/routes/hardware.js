const getData = require('../utils/get-raw-data')
const crud = require('../utils/crud')

const baseRoute = '/v2/hardware'

module.exports = function(instance) {
  return {
    ...crud(instance, baseRoute),

    getHardware(type, config = {}) {
      return instance
        .get(`${baseRoute}/${type}`, config)
        .then(getData(config.raw))
    },

    createHardware(type, body, config = {}) {
      return instance
        .post(`${baseRoute}/${type}`, body, config)
        .then(getData(config.raw))
    },

    deleteHardware(type, id, config = {}) {
      return instance.delete(`${baseRoute}/${type}/${id}`, config)
    },
  }
}
