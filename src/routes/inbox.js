const crud = require('../utils/crud')
const getData = require('../utils/get-raw-data')

const baseRoute = '/v2/inbox'

function createInboxRoutes(instance, suffix) {
  return {
    get(userId, config = {}) {
      return instance.get(`${baseRoute}/${userId}/${suffix}`, config)
        .then(getData(config.raw))
    },

    view(userId, body, config) {
      return instance.post(
        `${baseRoute}/${userId}/${suffix}/view`,
        body,
        config
      )
    }
  }
}

module.exports = function(instance) {
  return {
    documents: createInboxRoutes(instance, 'documents'),
    newsflashes: createInboxRoutes(instance, 'newsflashes'),
  }
}
