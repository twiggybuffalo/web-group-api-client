const crud = require('../utils/crud')
const getData = require('../utils/get-raw-data')
const baseRoute = '/v2/content'

function lessonRoutes(instance, type) {
  return {
    ...crud(instance, `${baseRoute}/${type}/lessons`),

    changeState(id, config) {
      const { from, to, ...data } = config
      return instance.post(
        `${baseRoute}/${type}/lessons/${id}/state?from=${from}&to=${to}`,
        data
      )
    },

    assessments: {
      ...crud(instance, `${baseRoute}/${type}/assessments`),

      putQuestions(id, body = {}, config = {}, extractNestedData = true) {
        return instance
          .put(
            `${baseRoute}/${type}/assessments/${id}/questions`,
            body,
            config
          )
          .then(getData(!extractNestedData))
      },
    },
  }
}

module.exports = function(instance) {
  return {
    onboarding: lessonRoutes(instance, 'onboarding'),
    formal: lessonRoutes(instance, 'formal'),
  }
}
