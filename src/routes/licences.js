const crud = require('../utils/crud')
const getData = require('../utils/get-raw-data')

const baseRoute = '/v2/licences'

module.exports = function(instance) {
  return {
    content: crud(instance, `${baseRoute}/content`),

    ...crud(instance, baseRoute),

    updateLicence(config = {}) {
      const { id, type, body, ...params } = config
      return instance
        .post(`${baseRoute}/${id}/${type}`, body, params)
        .then(getData(config.raw))
    },

    activateContentLicence(config = {}) {
      const { id, body, ...params } = config
      return instance.post(`${baseRoute}/content/${id}/activate`, body, params)
    },

    licenceLearnerCount(params = {}, config = {}) {
      config.params = params
      return instance
        .get(`${baseRoute}/learner/count`, config)
        .then(getData(config.raw))
    },

    licenceHardwareCount(params = {}, config = {}) {
      config.params = params
      return instance
        .get(`${baseRoute}/hardware/count`, config)
        .then(getData(config.raw))
    },

    accountLicenceHardwareCount(accountId, config = {}) {
      return instance
        .get(`/v2/hardware/devices/state?accountId=${accountId}`)
        .then(getData(config.raw))
    },

    getExpiring(type, config) {
      return instance
        .get(`${baseRoute}/${type}/count/expiring`, config)
        .then(getData(config.raw))
    },
  }
}
