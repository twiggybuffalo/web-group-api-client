const crud = require('../utils/crud')

const baseRoute = 'v2/marketplace'

const listingsRoute = `${baseRoute}/listings`
const vendorsRoute = `${baseRoute}/vendors`

module.exports = function(instance) {
  return {
    listings: {
      ...crud(instance, listingsRoute),

      filters: (config = {}) =>
        instance.get(`${listingsRoute}/filters`, config),

      search: (body = {}, config = {}) =>
        instance.post(`${listingsRoute}/search`, body, config),

      availability: (body = {}, config = {}) =>
        instance.post(`${listingsRoute}/availability`, body, config),

      featured: (config = {}) =>
        instance.get(`${listingsRoute}/featured`, config),

      contentRequest: (id, config = {}) =>
        instance.post(`${listingsRoute}/${id}/request`, config),
    },

    vendors: crud(instance, vendorsRoute),

    categories: (config = {}) =>
      instance.get(`${baseRoute}/categories/stats`, config),

    getLesson: (id, config = {}) =>
      instance.get(`${baseRoute}/lessons/${id}`, config),

    getCourse: (id, config = {}) =>
      instance.get(`${baseRoute}/courses/${id}`, config),
  }
}
