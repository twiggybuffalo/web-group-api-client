const getData = require('../utils/get-data')

module.exports = function(instance) {
  return {
    find(subjectId, config = {}) {
      return instance
        .get(`/v2/inbox/${subjectId}/messages`, config)
        .then(getData)
    },

    get(subjectId, messageId, config = {}) {
      // NOTE:  getting the message has a side-effect of marking it as read,
      //        thus we use a post request.
      return instance
        .post(`/v2/inbox/${subjectId}/messages/${messageId}`, config)
        .then(getData)
    },

    markAllRead(subjectId) {
      return instance.post(`/v2/inbox/${subjectId}/messages/read`).then(getData)
    },
  }
}
