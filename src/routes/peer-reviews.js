const crud = require('../utils/crud')

const baseRoute = '/v2/reviews'

module.exports = function(instance) {
  return crud(instance, baseRoute)
}
