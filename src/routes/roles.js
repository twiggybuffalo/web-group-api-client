const crud = require('../utils/crud')
const getData = require('../utils/get-data')

const baseRoute = '/v2/roles'

module.exports = instance => ({
  ...crud(instance, baseRoute),
  getAssignedRoles(config = {}) {
    return instance.get(`${baseRoute}/assigned`, config).then(getData)
  },
  enrollUser(data) {
    return instance.post(`${baseRoute}/enroll`, data).then(getData)
  },
  unenrollUser(data) {
    return instance.post(`${baseRoute}/unenroll`, data).then(getData)
  },
  changeState(id, config) {
    const { from, to, ...data } = config
    return instance.post(`${baseRoute}/${id}/state?from=${from}&to=${to}`, data)
  },
})
