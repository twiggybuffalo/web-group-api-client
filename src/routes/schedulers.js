const crud = require('../utils/crud')

const baseRoute = '/v2/schedulers'

module.exports = instance => ({
  ...crud(instance, baseRoute),
})
