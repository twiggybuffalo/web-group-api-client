const crud = require('../utils/crud')
const getData = require('../utils/get-data')
const baseRoute = '/v2/sites'

module.exports = instance => ({
  ...crud(instance, baseRoute),

  filters(accountId) {
    return instance
      .get(`${baseRoute}/filters`, {
        params: {
          accountId,
        },
      })
      .then(getData)
  },

  activate(id) {
    return instance.post(`${baseRoute}/${id}/activate`)
  },

  deactivate(id) {
    return instance.post(`${baseRoute}/${id}/deactivate`)
  },

  assignLearner(siteId, userIds) {
    return instance.post(`${baseRoute}/${siteId}/assign`, { userIds })
  },

  unassignLearner(siteId, userIds) {
    return instance.post(`${baseRoute}/${siteId}/unassign`, { userIds })
  },

  assigned(config) {
    return instance.get(`${baseRoute}/assigned`, config).then(getData)
  },

  assignedUsers(id, config) {
    return instance.get(`${baseRoute}/${id}/users`, config).then(getData)
  },

  assignedById(id, config) {
    return instance.get(`${baseRoute}/assigned/${id}`, config).then(getData)
  },
})
