const getData = require('../utils/get-raw-data')
const crud = require('../utils/crud')

module.exports = function(instance) {
  return {
    ...crud(instance, '/v2/contacts/standard'),

    getContacts(config = {}) {
      return instance
        .get('/v2/contacts/standard', config)
        .then(getData(config.raw))
    },
  }
}
