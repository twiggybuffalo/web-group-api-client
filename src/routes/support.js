const crud = require('../utils/crud')
const getData = require('../utils/get-raw-data')

const baseRoute = '/v2/support'

module.exports = function(instance) {
  return {
    faq: {
      ...crud(instance, `${baseRoute}/faqs`),
    },

    glossary: {
      ...crud(instance, `${baseRoute}/terms`),
    },

    manuals: {
      ...crud(instance, `${baseRoute}/manuals`),
    },

    videos: {
      ...crud(instance, `${baseRoute}/videos`),
    },

    steps: {
      ...crud(instance, `${baseRoute}/steps`),
    },

    orderedFiles: {
      ...crud(instance, `${baseRoute}/orderedfiles`),
    },

    downloadMedia: {
      get(uri, config = {}) {
        return instance.get(uri, config).then(getData(config.raw))
      },
    },
  }
}
