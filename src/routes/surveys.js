const crud = require('../utils/crud')
const getData = require('../utils/get-raw-data')

const baseRoute = '/v2/content/surveys'

module.exports = function(instance) {
  return {
    ...crud(instance, baseRoute),

    getCount(accountId, config = {}) {
      return instance
        .get(`${baseRoute}/count?accountId=${accountId}`, config)
        .then(getData(config.raw))
    },

    editQuestions(id, data) {
      return instance.put(`${baseRoute}/${id}/questions`, data)
    },

    changeState(id, config) {
      const { from, to, ...data } = config
      return instance.post(
        `${baseRoute}/${id}/state?from=${from}&to=${to}`,
        data
      )
    },
  }
}
