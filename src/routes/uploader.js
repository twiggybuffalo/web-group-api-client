const baseUrl = '/v2/uploader'

module.exports = function(instance) {
  return {
    bulkUploadLearners(accountId, data, config) {
      return instance.put(`${baseUrl}/learners/${accountId}`, data, config)
    },
  }
}
