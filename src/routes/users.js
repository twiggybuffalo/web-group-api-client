const getData = require('../utils/get-raw-data')
const crud = require('../utils/crud')

const baseRoute = '/v2/users'

module.exports = function(instance) {
  return {
    ...crud(instance, baseRoute),

    getGroupsByUserById(userId, config = {}) {
      return instance
        .get(`${baseRoute}/${userId}/groups`, config)
        .then(getData(config.raw))
    },

    getUserCount(config = {}) {
      return instance
        .get(`${baseRoute}/count`, config)
        .then(getData(config.raw))
    },

    addRoleToLearner(learnerId, roles) {
      return instance.post(`${baseRoute}/${learnerId}/roles`, roles)
    },

    removeRoleFromLearner(learnerId, roles) {
      return instance.delete(`${baseRoute}/${learnerId}/roles`, { data: roles })
    },

    activate(id, activate = true, config = {}) {
      const action = activate ? 'activate' : 'deactivate'
      return instance
        .post(`${baseRoute}/${id}/${action}`)
        .then(getData(config.raw))
    },

    removeLearnerRole(learnerId, config = {}) {
      return instance.delete(
        `${baseRoute}/${learnerId}/roles`,
        { data: ['user:learner'] },
        config
      )
    },
  }
}
