const etagMap = new Map()

function isCacheableMethod(method = '') {
  return ['GET', 'HEAD'].includes(method.toUpperCase())
}

function isIfMatchMethod(method) {
  return ['PUT', 'PATCH'].includes(method.toUpperCase())
}

function axiosIfMatchCacheIntercept(axios) {
  axios.interceptors.request.use(config => {
    if (isIfMatchMethod(config.method)) {
      const { url } = config
      const etag = etagMap.get(`/api${url}`)

      if (etag) {
        config.headers['If-Match'] = etag.replace(new RegExp('"', 'g'), '')
        config.headers['Content-Type'] = 'application/merge-patch+json'
      }
    }

    return config
  })

  // Add a response interceptor
  axios.interceptors.response.use(response => {
    if (isCacheableMethod(response.config.method)) {
      const { etag } = response.headers
      if (etag) {
        etagMap.set(response.config.url, etag)
      }
    }
    return response
  })

  return axios
}

module.exports = {
  axiosIfMatchCacheIntercept,
  clear() {
    return etagMap.clear()
  },
  set(url, etag) {
    etagMap.set(url, etag)
  },
  get(url) {
    return etagMap.get(`/api${url}`)
  },
}
