const getData = require('../utils/get-raw-data')

function crud(instance, baseRoute) {
  return {
    query(params, config = {}) {
      const { query } = params
      if (query) {
        return instance.get(`${baseRoute}/search`, { params, ...config })
      }
      return instance.get(baseRoute, { params, ...config })
    },

    getById(id, config = {}, extractNestedData = true) {
      return instance
        .get(`${baseRoute}/${id}`, config)
        .then(getData(!extractNestedData))
    },

    getMultiple(ids = [], config = {}, extractNestedData = true) {
      return Promise.all(
        ids.map(id =>
          instance
            .get(`${baseRoute}/${id}`, config)
            .then(getData(!extractNestedData))
        )
      )
    },

    create(body = {}, config = {}, extractNestedData = true) {
      return instance
        .post(baseRoute, body, config)
        .then(getData(!extractNestedData))
    },

    createMultiple(objects = [], config = {}, extractNestedData = true) {
      return Promise.all(
        objects.map(obj =>
          instance
            .post(baseRoute, obj, config)
            .then(getData(!extractNestedData))
        )
      )
    },

    patch(id, body = {}, prevConfig = {}, extractNestedData = true) {
      const config = {
        headers: {
          'Content-Type': 'application/merge-patch+json',
        },
        ...prevConfig,
      }

      return instance
        .patch(`${baseRoute}/${id}`, body, config)
        .then(getData(!extractNestedData))
    },

    put(id, body = {}, config = {}, extractNestedData = true) {
      return instance
        .put(`${baseRoute}/${id}`, body, config)
        .then(getData(!extractNestedData))
    },

    delete(id) {
      return instance.delete(`${baseRoute}/${id}`)
    },

    deleteMultiple(ids = []) {
      return Promise.all(ids.map(id => instance.delete(`${baseRoute}/${id}`)))
    },

    export(exportAs, params, type) {
      const exportType = type ? `?type=${type}` : ''
      return instance.post(
        `${baseRoute}/export/${exportAs}${exportType}`,
        params
      )
    },
  }
}

module.exports = crud
