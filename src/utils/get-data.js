module.exports = function getData(res) {
  const data = res.data

  if (data.data) {
    return data.data
  }

  return data
}
