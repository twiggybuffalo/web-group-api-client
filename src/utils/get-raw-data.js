module.exports = function getData(raw = false) {
  return res => {
    if (raw) {
      return res
    }

    const data = res.data

    if (data.data || data.data === 0) {
      return data.data
    }

    return data
  }
}
