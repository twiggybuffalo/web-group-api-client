function axiosTokenIntercept(axios) {
  axios.interceptors.request.use(config => {
    if (typeof window !== 'undefined') {
      const token = window.localStorage.getItem('kc_token')

      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
    }

    return config
  })

  return axios
}

module.exports = axiosTokenIntercept
